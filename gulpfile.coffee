
gulp         = require("gulp")
jade         = require("gulp-jade")
watch        = require("gulp-watch")
stylus       = require("gulp-stylus")
coffee       = require("gulp-coffee")
prefix       = require("gulp-autoprefixer")
uglify       = require("gulp-uglify")
csso         = require("gulp-csso")
include      = require("gulp-include")
addsrc       = require("gulp-add-src")
concat       = require("gulp-concat")
imagemin     = require("gulp-imagemin")
pngquant     = require("imagemin-pngquant")
livereload   = require("gulp-livereload")
changed      = require("gulp-changed")

path_dev     = "dev"
path_dist    = "dist"
path_prod	 = "site/public"


files =
	html :
		src : path_dev + "/jade/template.jade"
		watch : path_dev + "/jade/**/*.jade"
		dest : path_dist

	css :
		src : path_dev + "/stylus/main.styl"
		watch : path_dev + "/stylus/**/*.styl"
		dest : path_dist + "/css"
		dest2 : path_prod + "/css"


	js :
		src : path_dev + "/coffee/main.coffee"
		watch : path_dev + "/coffee/**/*.coffee"
		modulesCustom : path_dev + "/coffee/modulesCustom/*.coffee"
		plugins : ["bower_components/jquery/dist/jquery.min.js","bower_components/imagesloaded/imagesloaded.pkgd.min.js","bower_components/jquery-validation/dist/jquery.validate.min.js"]
		dest : path_dist + "/js"
		dest2 : path_prod + "/js"


	images:
		watch:          path_dist + "/img/**/*"
		src:            path_dist + "/img/**/*"
		destDist:       path_dist + "/img"
		destDist2:      path_prod + "/img"


gulp.task "default", ->

	livereload.listen()
	gulp.watch("dist/**/*.css").on "change", livereload.changed
	
	gulp.watch files.html.watch,    [ "build-html" ]
	gulp.watch files.css.watch,     [ "build-css" ]
	gulp.watch files.js.watch,      [ "build-js" ]
	gulp.watch files.images.watch,  [ "build-images" ]
	return
 
gulp.task 'build', [
	'build-html'
	'build-js'
	'build-css'
	'build-images'
]

gulp.task "build-html", ->
	gulp.src(files.html.src)
		.pipe(jade(pretty: true))
		.pipe(gulp.dest(files.html.dest))
	return

gulp.task "build-css", ->
	gulp.src(files.css.src)
		.pipe(stylus('include css': true))
		.pipe(csso({ comments: false }))
		.pipe(prefix())
		.pipe(gulp.dest(files.css.dest))
		.pipe(gulp.dest(files.css.dest2))
	return

gulp.task "build-js", ->
	gulp.src(files.js.src)
		.pipe(include())
		.pipe(addsrc.append(files.js.modulesCustom))
		.pipe(coffee(bare: true))
		.pipe(addsrc.prepend(files.js.plugins))
		.pipe(concat('main.js'))
		.pipe(uglify())
		.pipe(gulp.dest(files.js.dest))
		.pipe(gulp.dest(files.js.dest2))
	return

gulp.task "build-images", ->
	gulp.src(files.images.src)
		.pipe(changed(path_dev,extension: ".jpg|.png|.gif"))
		.pipe(imagemin( progressive: true, use: [ pngquant(quality: "70-75") ] ))
		.pipe(gulp.dest(files.images.destDist))
		.pipe(gulp.dest(files.images.destDist2))
	return