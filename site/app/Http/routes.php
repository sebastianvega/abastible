<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('search/{id}', 'HomeController@search');
Route::post('save_user_and_idea/','HomeController@savePost');
Route::get('vote/{idea_id}','HomeController@saveVote');
/* ================== Homepage + Admin Routes ================== */

require __DIR__.'/admin_routes.php';
