<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\Idea;
use App\Models\Vote;
use App\Models\Employee;

use App\User;
use Validator;
use App\Role;
use Eloquent;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Foundation\Validation\ValidatesRequests;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */

class HomeController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// Perform CSRF check on all post/put/patch/delete requests
	    

	}

	/**
	 * Show the application dashboard.
	 *
	 * @return Response
	 */
	public function index()
	{
		$roleCount = \App\Role::count();
		if($roleCount != 0) {
			if($roleCount != 0) {
				$ideas = Idea::where('state','show')->orderBy("created_at", 'desc')->get();
				return view('home',['ideas'=>$ideas]);
			}
		} else {
			return view('errors.error', [
				'title' => 'Migration not completed',
				'message' => 'Please run command <code>php artisan db:seed</code> to generate required table data.',
			]);
		}
	}



	public function search($search){
		
		$employees = Employee::where('name','LIKE','%'.$search.'%')->get();
		$employees = $employees->toArray();
		$ideas = Idea::whereIn('user_id',array_column($employees, 'id'))->where('state','show')->get()->toArray();
		foreach ($ideas as $key => $idea):
			$ideas[$key]['user_id'] = ['id'=>$idea['id'], 'name'=>Employee::where('id',$idea['user_id'])->first()->name];
		endforeach;

		return response()->json($ideas);
	}


	public function savePost(Request $request){
		$employee = Employee::where('email',$request->input('email'))->first();
		//$employee = Employee::where('email','asdasd@asdad.com')->first();
       	if (is_null($employee)) {
	       	$employee = Employee::create([
	            'name' => $request->input('name'),
	            'designation' => "user_front",
	            'mobile' => "8888888888",
	            'mobile2' => "",
	            'email' => $request->input('email'),
	            'gender' => '',
	            'dept' => 3,
	            'city' => "",
	            'address' => "",
	            'about' => "Usuario que genera ideas",
	            'date_birth' => date("Y-m-d"),
	            'date_hire' => date("Y-m-d"),
	            'date_left' => date("Y-m-d"),
	            'salary_cur' => 0,
	            'rut' =>$request->input('rut'),
	            'frequency_purchase' =>$request->input('clienteFrecuente'),
	        ]);
        
	        $user = User::create([
	            'name' => $request->input('name'),
	            'email' => $request->input('email'),
	            'password' => bcrypt(uniqid()),
	            'context_id' => $employee->id,
	            'type' => "Employee",
	        ]);
	        $role = Role::where('name', 'FRONTUSER')->first();
	        $user->attachRole($role);
       	}

		$idea = New Idea();
		$idea->user_id = $employee->id;
		$idea->idea = $request->input('message');
		$idea->state = "hidden";
		$idea->score = 0;
		$idea->save();
		return response()->json(['success' =>true]);
	}



	public function saveVote($idea_id){

		$vote = Vote::where('idea_id',$idea_id)->where('ip',$_SERVER['REMOTE_ADDR'])->first();
		if (!$vote) {
			$idea = Idea::where('id',$idea_id)->first();
			$idea->update(['score' => $idea->score+1]);


			$vote = new Vote;
			$vote->idea_id = $idea_id;
			$vote->ip = $_SERVER['REMOTE_ADDR'];
			$vote->save();
		}

		return response()->json(['success' =>true]);



	}
}