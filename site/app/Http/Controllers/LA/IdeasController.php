<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Idea;

class IdeasController extends Controller
{
	public $show_action = true;
	public $view_col = 'user_id';
	public $listing_cols = ['id', 'user_id','idea', 'state',  'score'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Ideas', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Ideas', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Ideas.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Ideas');
		
		if(Module::hasAccess($module->id)) {
			return View('la.ideas.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Show the form for creating a new idea.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created idea in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(Module::hasAccess("Ideas", "create")) {
		
			$rules = Module::validateRules("Ideas", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$insert_id = Module::insert("Ideas", $request);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.ideas.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified idea.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Ideas", "view")) {
			
			$idea = Idea::find($id);
			if(isset($idea->id)) {
				$module = Module::get('Ideas');
				$module->row = $idea;
				
				return view('la.ideas.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('idea', $idea);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("idea"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for editing the specified idea.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Ideas", "edit")) {			
			$idea = Idea::find($id);
			if(isset($idea->id)) {	
				$module = Module::get('Ideas');
				
				$module->row = $idea;
				
				return view('la.ideas.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('idea', $idea);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("idea"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified idea in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Ideas", "edit")) {
			
			$rules = Module::validateRules("Ideas", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Ideas", $request, $id);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.ideas.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified idea from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Ideas", "delete")) {
			Idea::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.ideas.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('ideas')->select($this->listing_cols)->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Ideas');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/ideas/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				$output .= '<a title="cambiar estado" href="#" data-id="'.$data->data[$i][0].'" class="btn btn-success btn-xs edit_state" style="display:inline;padding:2px 5px 3px 5px; margin: 0 5px 0 0"><i class="fa fa-edit"></i></a>';
				if(Module::hasAccess("Ideas", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/ideas/'.$data->data[$i][0].'/edit').'" data class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				/*if(Module::hasAccess("Ideas", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.ideas.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}*/
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}


	public function changeState(Request $request){
		$idea = Idea::where('id',$request->input('idea_id'))->first();

		if ($idea->state =="show") $idea->update(['state' => 'hidden']);
		else $idea->update(['state' => 'show']);
		
		return response()->json(['success' =>true,'state'=>$idea->state]);
	}
}
