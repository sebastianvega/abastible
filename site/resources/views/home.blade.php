<!DOCTYPE html>
<html>
  <head>
    <title>Abastible</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
    <script src="{{ asset('js/main.js') }}" type="text/javascript"></script>
    <!-- Meta tags facebook-->
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="">
    <!-- Meta tags twitter cards-->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="">
    <!--[if lt IE 9]> <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> <![endif]-->
  </head>
  <body data-api-url="{{ getenv('APP_URL') }}">
    <header>
      <div class="limit">
        <div class="header-content">
          <div class="header-logo"><a href=""><img src="images/logo.png" class="logo"></a></div>
          <div class="header-hamburger">
            <div class="fa1"></div>
            <div class="fa2"></div>
            <div class="fa3"></div>
          </div>
          <nav>
            <div class="fa fa-times"></div>
            <ul>
              <li><a href="#slide">Inicio</a></li>
              <li><a href="#text-aba">Participa aquí</a></li>
              <li><a href="#ideas-direct">Ideas</a></li>
              <li><a target="_blank" href="{{ getenv('APP_URL') }}/MODELO_BASES_DE_CONCURSO_05_06_FINAL.pdf">Bases legales</a></li>
            </ul>
          </nav>
        </div>
      </div>
    </header>
    <div class="slide" id="slide">
      <div data-id="1" class="imagen slide-imagen bg current">
        <img src="images/fondo-clean.png">
      </div>
      <div data-id="2" class="imagen slide-imagen bg">
        <img src="images/fondo-clean.png">
      </div>
      <div data-id="3" class="imagen slide-imagen bg">
        <img src="images/fondo-clean.png">
      </div>
      <span class="bullets">
          <div data-id="1" class="bullet current"></div>
          <div data-id="2" class="bullet"></div>
          <div data-id="3" class="bullet"></div>
        </span>
      <div class="content">
        <div class="limit">
          <div class="slide-text">
            <h1>Mejorando el mundo, ganamos todos</h1>
            <h2 class="hashtag">#MejoremosElMundo</h2>
            <p>Cuéntanos qué haces para que tu barrio sea un mejor lugar para las futuras generaciones.</p><a href="#text-aba"><img src="images/boton-participa-aqui.png" class="logo-orange"></a>
          </div>
        </div>
      </div>
    </div>
    <div class="contact" id="contact">
      <div class="limit">
        <div class="cols sub-contact cols-gutter-0">
          <div class="sub-contact-text col-xs-7 dscroll">
            <h2 class="hashtag">#MejoremosElMundo</h2>
            <p id="text-aba"><strong>Abastible elegirá entre las ideas más votadas a los 3 ganadores de estos increíbles premios.</strong></p><br>
            <strong>PREMIOS: </strong>
            <ul>
              <li>1er lugar: Gift Card $500.000</li>
              <li>2do lugar: Gift Card $100.000</li>
              <li>3er lugar: Gift Card $50.000</li>
            </ul>
          </div>
          <div class="sub-contact-acept col-xs-5"><img class="dscroll" src="images/acept.png"></div>
        </div>
      </div>
      <div class="contact-form limit ">
        <form action="{{ url('save_user_and_idea/') }}" method="post">
        {{ csrf_field() }}
          <div class="form-group dscroll">
            <input type="text" placeholder="Nombre" name="name">
          </div>
          <div class="form-group dscroll">
            <input type="text" placeholder="Rut" name="rut">
          </div>
          <div class="form-group dscroll">
            <input type="email" placeholder="Email" name="email">
          </div>
          <div class="radio dscroll">
            <div class="radio-title">¿Eres cliente frecuente de Abastible?</div>
            <div class="radio-radio first-radio">
              <label for="yes">Si  </label>
              <input type="radio" name="clienteFrecuente" id="yes" value="Si">
            </div>
            <div class="radio-radio">
              <label for="no">No</label>
              <input type="radio" name="clienteFrecuente" id="no" value="No">
            </div>
          </div>
          <div class="form-group dscroll">
            <input type="text" placeholder="Cuéntanos aquí qué haces para ayudar a tu barrio" name="message" maxlength="300">
          </div>
          <button type="submit" class="icn-next contact-button dscroll">Enviar</button>
        </form>
      </div>
      <div class="contact-mensaje">
      </div>
      <div class="ideas-direct" id="ideas-direct">
        <div class="ideas-direct-line"><img src="images/line-orange.png"></div>
        <div class="ideas-direct-text"><span><a href="#ideas-direct">Mira y vota por la idea que más te gusta<img src="images/arrow-down-orange.png"></a></span></div>
      </div>
    </div>
    <div class="best-ideas" id="ideas">
      <div class="limit">
        <div class="cols cols-gutter-0">
          <div class="col-sm-12 col-md-6 best-ideas-header">
            <div class="container-fluid"><img class="dscroll" src="images/bordered-acept.png">
              <h2>#MejoremosElMundo</h2>
            </div>
          </div>
          <div class="col-sm-12 col-md-6 best-ideas-header">
            <div class="container-fluid search-box">
              <form action="{{ url('search') }}" action="GET">
                <input type="text"  name="s" placeholder="Buscar participante">
              </form>
            </div>
          </div>
            @if ($ideas)
              @foreach ($ideas as $key => $idea)
                 <div class="col-sm-12 col-md-6 idea-container dscroll">
                  <article class="idea">
                    <div class="idea-autor">
                      <div class="idea-autor-name">{{ $idea->employee()->name }}</div><img src="images/quotes.png"/>
                    </div>
                    <p class="idea-body">{!! $idea->idea !!}</p>
                      <div class="idea-vote">
                          <a href="{{  url('vote') }} " data-id="{{ $idea->id }}" class="vote">Votar
                              <div class="icons"><img src="images/orange-acept.png"/><img src="images/orange-acept.png" class="icon2"/></div>
                          </a>
                      </div>
                  </article>
                </div>
              @endforeach
            @endif
        </div>
      </div>
    </div>
    <footer></footer>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
     
      ga('create', 'UA-3820742-2', 'auto');
      ga('send', 'pageview');
     
    </script>
  </body>
</html>
