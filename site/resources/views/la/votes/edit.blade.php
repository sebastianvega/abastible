@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ secure_url(config('laraadmin.adminRoute') . '/votes') }}">Vote</a> :
@endsection
@section("contentheader_description", $vote->$view_col)
@section("section", "Votes")
@section("section_url", secure_url(config('laraadmin.adminRoute') . '/votes'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Votes Edit : ".$vote->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($vote, ['route' => [config('laraadmin.adminRoute') . '.votes.update', $vote->id ], 'method'=>'PUT', 'id' => 'vote-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'idea_id')
					@la_input($module, 'ip')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ secure_url(config('laraadmin.adminRoute') . '/votes') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#vote-edit-form").validate({
		
	});
});
</script>
@endpush
