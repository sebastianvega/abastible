@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ secure_url(config('laraadmin.adminRoute') . '/ideas') }}">Idea</a> :
@endsection
@section("contentheader_description", $idea->$view_col)
@section("section", "Ideas")
@section("section_url", secure_url(config('laraadmin.adminRoute') . '/ideas'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Ideas Edit : ".$idea->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($idea, ['route' => [config('laraadmin.adminRoute') . '.ideas.update', $idea->id ], 'method'=>'PUT', 'id' => 'idea-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'user_id')
					@la_input($module, 'idea')
					@la_input($module, 'state')
					@la_input($module, 'score')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ secure_url(config('laraadmin.adminRoute') . '/ideas') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#idea-edit-form").validate({
		
	});
});
</script>
@endpush
