@extends("la.layouts.app")

@section("contentheader_title", "Ideas")
@section("contentheader_description", "Ideas listing")
@section("section", "Ideas")
@section("sub_section", "Listing")
@section("htmlheader_title", "Ideas Listing")

@section("headerElems")
@la_access("Ideas", "create")
	<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">Add Idea</button>
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			@foreach( $listing_cols as $col )
			<th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
			@endforeach
			@if($show_actions)
			<th>Actions</th>
			@endif
		</tr>
		</thead>
		<tbody>
			
		</tbody>
		</table>
	</div>
</div>

@la_access("Ideas", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Idea</h4>
			</div>
			{!! Form::open(['action' => 'LA\IdeasController@store', 'id' => 'idea-add-form']) !!}
			<div class="modal-body">
				<div class="box-body">
                    @la_form($module)
					
					{{--
					@la_input($module, 'idea')
					@la_input($module, 'state')
					@la_input($module, 'user_id')
					@la_input($module, 'score')
					--}}
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endla_access

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ secure_asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ secure_asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
$(function () {
	$("#example1").DataTable({
		processing: true,
        serverSide: true,
        ajax: "{{ secure_url(config('laraadmin.adminRoute') . '/idea_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});
	$("#idea-add-form").validate({
		
	});

	$( document ).on( "click", ".edit_state", function(event) {
		event.preventDefault();
		var $this =$(this);
		$.post(
			"{{ secure_url(config('laraadmin.adminRoute') . '/change_state/') }}",
			{"_token": "{{ csrf_token() }}",'idea_id':$(this).attr('data-id')}
		).done(function(result){
			console.log(result);
			$this.closest('tr').find('td').eq(3).html(result.state)
		});

	});
});
</script>
@endpush
