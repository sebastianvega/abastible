validaRut = (campo) ->
	if campo.length == 0
		return false
	if campo.length < 8
		return false
	campo = campo.replace('-', '')
	campo = campo.replace(/\./g, '')
	suma = 0
	caracteres = '1234567890kK'
	contador = 0
	i = 0
	while i < campo.length
		u = campo.substring(i, i + 1)
		if caracteres.indexOf(u) != -1
			contador++
		i++
	if contador == 0
		return false
	rut = campo.substring(0, campo.length - 1)
	drut = campo.substring(campo.length - 1)
	dvr = '0'
	mul = 2
	i = rut.length - 1
	while i >= 0
		suma = suma + rut.charAt(i) * mul
		if mul == 7
			mul = 2
		else
			mul++
		i--
	res = suma % 11
	if res == 1
		dvr = 'k'
	else if res == 0
		dvr = '0'
	else
		dvi = 11 - res
		dvr = dvi + ''
	if dvr != drut.toLowerCase()
		false
	else
		true

jQuery.validator.addMethod 'rut', ((value, element) ->
	@optional(element) or validaRut(value)
), 'Revise el RUT'