$(document).ready ->
	### slider
	i=0
	$(".bullet").click ->
		i = ($(this).attr "data-id")-1
	setInterval ->
		if i==$(".slide-imagen").length
			i=0
		$(".bullet").removeClass "current"
		$(".slide-imagen").removeClass "current"
		$(".bullet").eq(i).addClass "current"
		$(".slide-imagen").eq(i).addClass "current"
		i++
	,4000
	$(".bullets .bullet").click ->
		$(".bullets .bullet").removeClass "current"
		$(this).addClass "current"
		currentSlide = $(this).attr "data-id"
		$(this).closest("#slide").find(".slide-imagen").removeClass("current")
		$(".slide-imagen[data-id="+currentSlide+"]").addClass "current"
    ###

	

	API_URL = $("body").attr("data-api-url")

	# pone imagen como fondo en contenedor
	$(".bg").each ->
		bg  = $(this)
		src = bg.find("img").attr("src")
		bg.css
			'background-image': 'url('+src+')'
		bg.imagesLoaded ->
			bg.addClass("bg-loaded")



	# VOTE IDEA

	$(document).on "click", ".idea-vote a", (e) ->
		e.preventDefault()
		idea = $(this).attr("data-id")
		icon = $(this).find(".icon2")
		icon.addClass("active")
		if !$(this).hasClass("gray")
			$(this).addClass("gray")
			$.ajax(
				method: 'GET'
				url: API_URL+'/vote/'+idea
			).done(->
				console.log 'Su mensaje ha sido enviado con éxito'
				return
			).fail(->
				console.log 'Ha ocurrido un error intentelo nuevamente'
				return
			)


	# SEND IDEA

	$('.contact-form form').submit (e) ->
		form = $(this)
		e.preventDefault()

		# Validation

		validForm = true

		if validaRut( form.find("input[name='rut']").val() )
			form.find("input[name='rut']").closest(".form-group").removeClass "wrong-rut"
		else
			form.find("input[name='rut']").closest(".form-group").addClass "wrong-rut"
			validForm = false

		if form.find("input[name='email']").val().length < 2
			validForm = false

		if form.find("input[name='name']").val().length < 2
			validForm = false

		if !form.find("input[name='clienteFrecuente']").val()
			validForm = false

		if form.find("input[name='message']").val().length < 2
			validForm = false

		# Send form

		if validForm
			$.ajax(
				method: 'POST'
				url:    API_URL+'/save_user_and_idea'
				data:    form.serialize()
			).done(->
				$(".contact-mensaje").html 'Su mensaje ha sido enviado con éxito, pronto será validado y publicado'
				$(".contact-mensaje").addClass "show"
				$(".contact-mensaje").append "<div class='close-message fa fa-times'></div>"
				$(".close-message").click ->
					$(this).closest(".contact-mensaje").removeClass "show"

				return
			).fail(->
				$(".contact-mensaje").html 'Ha ocurrido un error intentelo nuevamente'
				$(".contact-mensaje").addClass "show"
				$(".contact-mensaje").append "<div class='close-message fa fa-times'></div>"
				$(".close-message").click ->
					$(this).closest(".contact-mensaje").removeClass "show"
				return
			)
	


	# SEARCH IDEA

	$(".search-box form").submit (e) ->

		e.preventDefault()
		string = $(this).find("input").val()

		$.ajax(
			method: 'GET'
			url:    API_URL+'/search/'+string
		).done( (results) ->

			$(".idea-container").hide()

			for idea in results

				html = $("article.idea").eq(0).parent().clone()
				html.find(".idea-body").html(idea.idea)
				html.find(".idea-autor-name").html(idea.user_id.name)
				html.find(".idea-vote .vote").attr("data-id",idea.id)
				html.show()
				
				$(".best-ideas .limit .cols").append(html)

				console.log html

			return
		).fail(->
			console.log 'Ha ocurrido un error intentelo nuevamente'
			return
		)


	$(".header-hamburger").click (e) ->
		e.stopPropagation()
		$("nav").toggleClass "active"
	$(".fa.fa-times").click ->
		$("nav").toggleClass "active"
	$('html').on "click", ->
		if $("nav").hasClass "active"
			$("nav").removeClass "active"


	$('a[href^="#"]').on 'click', (event) ->
	  target = $($(this).attr('href'))
	  if target.length
	    event.preventDefault()
	    $('html, body').animate { scrollTop: target.offset().top-40 }, 500
	  return

	dscroll = ->
		scroll = $(window).scrollTop()	
		if $(".dscroll").length
			element_top_prev  = 0
			element_top_delay = 0
			$(".dscroll:visible:not(.dscroll-in)").each ->
				element = $(this)
				element_top = element.offset().top
				if scroll + $(window).height() > element_top + 150
					element.addClass "dscroll-in"

					if element_top == element_top_prev
						element_top_delay++
						delay = element_top_delay*0.2
						element.css
							'-webkit-animation-delay': delay+"s"
							'animation-delay': delay+"s"
					else
						element_top_delay=0


					element_top_prev = element_top


	# Mostrar en scroll

	$(window).scroll ->
		dscroll()