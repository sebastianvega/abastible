-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 21-07-2017 a las 21:39:34
-- Versión del servidor: 10.1.24-MariaDB
-- Versión de PHP: 7.0.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `abastible`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `backups`
--

CREATE TABLE `backups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `file_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `backup_size` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tags` varchar(1000) COLLATE utf8_unicode_ci NOT NULL DEFAULT '[]',
  `color` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `departments`
--

INSERT INTO `departments` (`id`, `name`, `tags`, `color`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Administration', '[]', '#000', NULL, '2017-07-10 21:34:20', '2017-07-10 21:34:20'),
(2, 'Manager', '[]', 'Red', NULL, '2017-07-10 22:13:01', '2017-07-10 22:13:01'),
(3, 'Usuarios del sitio', '[]', '#98FB98', NULL, '2017-07-11 00:26:37', '2017-07-11 00:26:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `employees`
--

CREATE TABLE `employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `designation` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Male',
  `mobile` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `mobile2` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dept` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `city` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `about` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_birth` date NOT NULL DEFAULT '1990-01-01',
  `date_hire` date NOT NULL,
  `date_left` date NOT NULL DEFAULT '1990-01-01',
  `salary_cur` decimal(15,3) NOT NULL DEFAULT '0.000',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `frequency_purchase` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rut` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `employees`
--

INSERT INTO `employees` (`id`, `name`, `designation`, `gender`, `mobile`, `mobile2`, `email`, `dept`, `city`, `address`, `about`, `date_birth`, `date_hire`, `date_left`, `salary_cur`, `deleted_at`, `created_at`, `updated_at`, `frequency_purchase`, `rut`) VALUES
(1, 'admin', 'Super Admin', 'Male', '+56942904817', '', 'claudio@growthy.cl', 1, 'Santiago', 'Avenida Italia 850, Providencia', 'About user / biography', '1986-01-11', '2017-07-10', '2017-07-10', '0.000', NULL, '2017-07-10 21:34:51', '2017-07-10 21:42:36', '', ''),
(13, 'Mauricio Marambio', 'Admin', 'Male', '+569800000000', '', 'mauricio.marambio@dyu.cl', 2, '', '', '', '1990-01-01', '1970-01-01', '1990-01-01', '0.000', NULL, '2017-07-12 20:34:00', '2017-07-12 20:34:17', '', ''),
(16, 'testds fdsf ', 'user_front', '', '8888888888', '', 'felipe@growthy.cl', 3, '', '', 'Usuario que genera ideas', '2017-07-12', '2017-07-12', '2017-07-12', '0.000', NULL, '2017-07-12 22:36:14', '2017-07-12 22:36:14', 'yes', '16.166.807'),
(17, 'Ddddddddddddddd', 'user_front', '', '8888888888', '', '33333@3333.com', 3, '', '', 'Usuario que genera ideas', '2017-07-12', '2017-07-12', '2017-07-12', '0.000', NULL, '2017-07-13 01:08:50', '2017-07-13 01:08:50', 'si', '33333333-3'),
(18, '899999999999999', 'user_front', '', '8888888888', '', '44444@555.com', 3, '', '', 'Usuario que genera ideas', '2017-07-12', '2017-07-12', '2017-07-12', '0.000', NULL, '2017-07-13 01:10:28', '2017-07-13 01:10:28', 'No', '44444444-4'),
(19, '555555555', 'user_front', '', '8888888888', '', '555555@55555.com', 3, '', '', 'Usuario que genera ideas', '2017-07-12', '2017-07-12', '2017-07-12', '0.000', NULL, '2017-07-13 01:11:07', '2017-07-13 01:11:07', 'Si', '55555555-5');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ideas`
--

CREATE TABLE `ideas` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `idea` text COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'hidden',
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `score` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `ideas`
--

INSERT INTO `ideas` (`id`, `deleted_at`, `created_at`, `updated_at`, `idea`, `state`, `user_id`, `score`) VALUES
(11, '2017-07-12 22:29:55', '2017-07-12 20:57:29', '2017-07-12 22:29:55', 'asdasdasdasd asd asd asdasda', 'show', 14, 0),
(16, NULL, '2017-07-12 22:36:15', '2017-07-22 01:39:21', 'asd asd sda', 'hidden', 16, 0),
(17, NULL, '2017-07-13 01:08:51', '2017-07-22 01:39:22', 'wfwefwefwef', 'hidden', 17, 1),
(18, NULL, '2017-07-13 01:10:28', '2017-07-22 01:39:22', '5675756756757567', 'hidden', 18, 1),
(19, NULL, '2017-07-13 01:11:08', '2017-07-22 01:39:23', 'ttttttttttttttttt', 'hidden', 19, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `la_configs`
--

CREATE TABLE `la_configs` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `section` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `la_configs`
--

INSERT INTO `la_configs` (`id`, `key`, `section`, `value`, `created_at`, `updated_at`) VALUES
(1, 'sitename', '', 'Abastible  Mejoremos el Mundo', '2017-07-10 21:34:22', '2017-07-10 22:05:53'),
(2, 'sitename_part1', '', 'Abastible', '2017-07-10 21:34:22', '2017-07-10 22:05:53'),
(3, 'sitename_part2', '', 'Mejoremos el mundo', '2017-07-10 21:34:22', '2017-07-10 22:05:53'),
(4, 'sitename_short', '', 'AB', '2017-07-10 21:34:23', '2017-07-10 22:05:53'),
(5, 'site_description', '', 'LaraAdmin is a open-source Laravel Admin Panel for quick-start Admin based applications and boilerplate for CRM or CMS systems.', '2017-07-10 21:34:23', '2017-07-10 22:05:53'),
(6, 'sidebar_search', '', '1', '2017-07-10 21:34:23', '2017-07-10 22:05:53'),
(7, 'show_messages', '', '1', '2017-07-10 21:34:23', '2017-07-10 22:05:53'),
(8, 'show_notifications', '', '1', '2017-07-10 21:34:23', '2017-07-10 22:05:53'),
(9, 'show_tasks', '', '1', '2017-07-10 21:34:23', '2017-07-10 22:05:53'),
(10, 'show_rightsidebar', '', '1', '2017-07-10 21:34:23', '2017-07-10 22:05:53'),
(11, 'skin', '', 'skin-white', '2017-07-10 21:34:23', '2017-07-10 22:05:53'),
(12, 'layout', '', 'fixed', '2017-07-10 21:34:23', '2017-07-10 22:05:53'),
(13, 'default_email', '', 'claudio@growthy.cl', '2017-07-10 21:34:23', '2017-07-10 22:05:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `la_menus`
--

CREATE TABLE `la_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fa-cube',
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'module',
  `parent` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `hierarchy` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `la_menus`
--

INSERT INTO `la_menus` (`id`, `name`, `url`, `icon`, `type`, `parent`, `hierarchy`, `created_at`, `updated_at`) VALUES
(2, 'Users', 'users', 'fa-group', 'module', 0, 2, '2017-07-10 21:34:20', '2017-07-11 21:42:36'),
(4, 'Departments', 'departments', 'fa-tags', 'module', 1, 1, '2017-07-10 21:34:20', '2017-07-11 21:42:36'),
(5, 'Employees', 'employees', 'fa-group', 'module', 1, 2, '2017-07-10 21:34:20', '2017-07-11 21:42:36'),
(6, 'Roles', 'roles', 'fa-user-plus', 'module', 1, 3, '2017-07-10 21:34:20', '2017-07-11 21:42:36'),
(8, 'Permissions', 'permissions', 'fa-magic', 'module', 1, 4, '2017-07-10 21:34:20', '2017-07-11 21:42:36'),
(9, 'Ideas', 'ideas', 'fa fa-lightbulb-o', 'module', 0, 1, '2017-07-11 20:42:06', '2017-07-11 21:42:36'),
(10, 'Employees', 'employees', 'fa-group', 'module', 0, 0, '2017-07-12 20:26:21', '2017-07-12 20:26:21'),
(11, 'Votes', 'votes', 'fa fa-angellist', 'module', 0, 0, '2017-07-22 01:27:05', '2017-07-22 01:27:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modules`
--

CREATE TABLE `modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name_db` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `view_col` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `controller` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fa_icon` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fa-cube',
  `is_gen` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `modules`
--

INSERT INTO `modules` (`id`, `name`, `label`, `name_db`, `view_col`, `model`, `controller`, `fa_icon`, `is_gen`, `created_at`, `updated_at`) VALUES
(1, 'Users', 'Users', 'users', 'name', 'User', 'UsersController', 'fa-group', 1, '2017-07-10 21:34:00', '2017-07-10 21:34:23'),
(2, 'Uploads', 'Uploads', 'uploads', 'name', 'Upload', 'UploadsController', 'fa-files-o', 1, '2017-07-10 21:34:02', '2017-07-10 21:34:23'),
(3, 'Departments', 'Departments', 'departments', 'name', 'Department', 'DepartmentsController', 'fa-tags', 1, '2017-07-10 21:34:03', '2017-07-10 21:34:23'),
(4, 'Employees', 'Employees', 'employees', 'name', 'Employee', 'EmployeesController', 'fa-group', 1, '2017-07-10 21:34:04', '2017-07-10 21:34:23'),
(5, 'Roles', 'Roles', 'roles', 'name', 'Role', 'RolesController', 'fa-user-plus', 1, '2017-07-10 21:34:06', '2017-07-10 21:34:23'),
(6, 'Organizations', 'Organizations', 'organizations', 'name', 'Organization', 'OrganizationsController', 'fa-university', 1, '2017-07-10 21:34:10', '2017-07-10 21:34:23'),
(7, 'Backups', 'Backups', 'backups', 'name', 'Backup', 'BackupsController', 'fa-hdd-o', 1, '2017-07-10 21:34:12', '2017-07-10 21:34:23'),
(8, 'Permissions', 'Permissions', 'permissions', 'name', 'Permission', 'PermissionsController', 'fa-magic', 1, '2017-07-10 21:34:13', '2017-07-10 21:34:23'),
(9, 'Ideas', 'Ideas', 'ideas', 'user_id', 'Idea', 'IdeasController', 'fa-lightbulb-o', 1, '2017-07-11 00:16:59', '2017-07-11 20:42:06'),
(10, 'Votes', 'Votes', 'votes', 'idea_id', 'Vote', 'VotesController', 'fa-angellist', 1, '2017-07-22 01:25:26', '2017-07-22 01:27:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `module_fields`
--

CREATE TABLE `module_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `colname` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `module` int(10) UNSIGNED NOT NULL,
  `field_type` int(10) UNSIGNED NOT NULL,
  `unique` tinyint(1) NOT NULL DEFAULT '0',
  `defaultvalue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `minlength` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `maxlength` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `popup_vals` text COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `module_fields`
--

INSERT INTO `module_fields` (`id`, `colname`, `label`, `module`, `field_type`, `unique`, `defaultvalue`, `minlength`, `maxlength`, `required`, `popup_vals`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'name', 'Name', 1, 16, 0, '', 5, 250, 1, '', 0, '2017-07-10 21:34:01', '2017-07-10 21:34:01'),
(2, 'context_id', 'Context', 1, 13, 0, '0', 0, 0, 0, '', 0, '2017-07-10 21:34:01', '2017-07-10 21:34:01'),
(3, 'email', 'Email', 1, 8, 1, '', 0, 250, 0, '', 0, '2017-07-10 21:34:01', '2017-07-10 21:34:01'),
(4, 'password', 'Password', 1, 17, 0, '', 6, 250, 1, '', 0, '2017-07-10 21:34:01', '2017-07-10 21:34:01'),
(5, 'type', 'User Type', 1, 7, 0, 'Employee', 0, 0, 0, '[\"Employee\",\"Client\"]', 0, '2017-07-10 21:34:01', '2017-07-10 21:34:01'),
(6, 'name', 'Name', 2, 16, 0, '', 5, 250, 1, '', 0, '2017-07-10 21:34:02', '2017-07-10 21:34:02'),
(7, 'path', 'Path', 2, 19, 0, '', 0, 250, 0, '', 0, '2017-07-10 21:34:02', '2017-07-10 21:34:02'),
(8, 'extension', 'Extension', 2, 19, 0, '', 0, 20, 0, '', 0, '2017-07-10 21:34:02', '2017-07-10 21:34:02'),
(9, 'caption', 'Caption', 2, 19, 0, '', 0, 250, 0, '', 0, '2017-07-10 21:34:02', '2017-07-10 21:34:02'),
(10, 'user_id', 'Owner', 2, 7, 0, '1', 0, 0, 0, '@users', 0, '2017-07-10 21:34:02', '2017-07-10 21:34:02'),
(11, 'hash', 'Hash', 2, 19, 0, '', 0, 250, 0, '', 0, '2017-07-10 21:34:03', '2017-07-10 21:34:03'),
(12, 'public', 'Is Public', 2, 2, 0, '0', 0, 0, 0, '', 0, '2017-07-10 21:34:03', '2017-07-10 21:34:03'),
(13, 'name', 'Name', 3, 16, 1, '', 1, 250, 1, '', 0, '2017-07-10 21:34:04', '2017-07-10 21:34:04'),
(14, 'tags', 'Tags', 3, 20, 0, '[]', 0, 0, 0, '', 0, '2017-07-10 21:34:04', '2017-07-10 21:34:04'),
(15, 'color', 'Color', 3, 19, 0, '', 0, 50, 1, '', 0, '2017-07-10 21:34:04', '2017-07-10 21:34:04'),
(16, 'name', 'Name', 4, 16, 0, '', 5, 250, 1, '', 0, '2017-07-10 21:34:04', '2017-07-10 21:34:04'),
(17, 'designation', 'Designation', 4, 19, 0, '', 0, 50, 1, '', 0, '2017-07-10 21:34:04', '2017-07-10 21:34:04'),
(18, 'gender', 'Gender', 4, 18, 0, 'Male', 0, 0, 1, '[\"Male\",\"Female\"]', 0, '2017-07-10 21:34:04', '2017-07-10 21:34:04'),
(19, 'mobile', 'Mobile', 4, 14, 0, '', 10, 20, 1, '', 0, '2017-07-10 21:34:04', '2017-07-10 21:34:04'),
(20, 'mobile2', 'Alternative Mobile', 4, 14, 0, '', 10, 20, 0, '', 0, '2017-07-10 21:34:04', '2017-07-10 21:34:04'),
(21, 'email', 'Email', 4, 8, 1, '', 5, 250, 1, '', 0, '2017-07-10 21:34:04', '2017-07-10 21:34:04'),
(22, 'dept', 'Department', 4, 7, 0, '0', 0, 0, 1, '@departments', 0, '2017-07-10 21:34:04', '2017-07-10 21:34:04'),
(23, 'city', 'City', 4, 19, 0, '', 0, 50, 0, '', 0, '2017-07-10 21:34:04', '2017-07-10 21:34:04'),
(24, 'address', 'Address', 4, 1, 0, '', 0, 1000, 0, '', 0, '2017-07-10 21:34:05', '2017-07-10 21:34:05'),
(25, 'about', 'About', 4, 19, 0, '', 0, 0, 0, '', 0, '2017-07-10 21:34:05', '2017-07-10 21:34:05'),
(26, 'date_birth', 'Date of Birth', 4, 4, 0, '1990-01-01', 0, 0, 0, '', 0, '2017-07-10 21:34:05', '2017-07-10 21:34:05'),
(27, 'date_hire', 'Hiring Date', 4, 4, 0, 'date(\'Y-m-d\')', 0, 0, 0, '', 0, '2017-07-10 21:34:05', '2017-07-10 21:34:05'),
(28, 'date_left', 'Resignation Date', 4, 4, 0, '1990-01-01', 0, 0, 0, '', 0, '2017-07-10 21:34:05', '2017-07-10 21:34:05'),
(29, 'salary_cur', 'Current Salary', 4, 6, 0, '0.0', 0, 2, 0, '', 0, '2017-07-10 21:34:05', '2017-07-10 21:34:05'),
(30, 'name', 'Name', 5, 16, 1, '', 1, 250, 1, '', 0, '2017-07-10 21:34:06', '2017-07-10 21:34:06'),
(31, 'display_name', 'Display Name', 5, 19, 0, '', 0, 250, 1, '', 0, '2017-07-10 21:34:06', '2017-07-10 21:34:06'),
(32, 'description', 'Description', 5, 21, 0, '', 0, 1000, 0, '', 0, '2017-07-10 21:34:06', '2017-07-10 21:34:06'),
(33, 'parent', 'Parent Role', 5, 7, 0, '1', 0, 0, 0, '@roles', 0, '2017-07-10 21:34:06', '2017-07-10 21:34:06'),
(34, 'dept', 'Department', 5, 7, 0, '1', 0, 0, 0, '@departments', 0, '2017-07-10 21:34:06', '2017-07-10 21:34:06'),
(35, 'name', 'Name', 6, 16, 1, '', 5, 250, 1, '', 0, '2017-07-10 21:34:10', '2017-07-10 21:34:10'),
(36, 'email', 'Email', 6, 8, 1, '', 0, 250, 0, '', 0, '2017-07-10 21:34:10', '2017-07-10 21:34:10'),
(37, 'phone', 'Phone', 6, 14, 0, '', 0, 20, 0, '', 0, '2017-07-10 21:34:10', '2017-07-10 21:34:10'),
(38, 'website', 'Website', 6, 23, 0, 'http://', 0, 250, 0, '', 0, '2017-07-10 21:34:10', '2017-07-10 21:34:10'),
(39, 'assigned_to', 'Assigned to', 6, 7, 0, '0', 0, 0, 0, '@employees', 0, '2017-07-10 21:34:10', '2017-07-10 21:34:10'),
(40, 'connect_since', 'Connected Since', 6, 4, 0, 'date(\'Y-m-d\')', 0, 0, 0, '', 0, '2017-07-10 21:34:10', '2017-07-10 21:34:10'),
(41, 'address', 'Address', 6, 1, 0, '', 0, 1000, 1, '', 0, '2017-07-10 21:34:10', '2017-07-10 21:34:10'),
(42, 'city', 'City', 6, 19, 0, '', 0, 250, 1, '', 0, '2017-07-10 21:34:10', '2017-07-10 21:34:10'),
(43, 'description', 'Description', 6, 21, 0, '', 0, 1000, 0, '', 0, '2017-07-10 21:34:10', '2017-07-10 21:34:10'),
(44, 'profile_image', 'Profile Image', 6, 12, 0, '', 0, 250, 0, '', 0, '2017-07-10 21:34:10', '2017-07-10 21:34:10'),
(45, 'profile', 'Company Profile', 6, 9, 0, '', 0, 250, 0, '', 0, '2017-07-10 21:34:10', '2017-07-10 21:34:10'),
(46, 'name', 'Name', 7, 16, 1, '', 0, 250, 1, '', 0, '2017-07-10 21:34:12', '2017-07-10 21:34:12'),
(47, 'file_name', 'File Name', 7, 19, 1, '', 0, 250, 1, '', 0, '2017-07-10 21:34:12', '2017-07-10 21:34:12'),
(48, 'backup_size', 'File Size', 7, 19, 0, '0', 0, 10, 1, '', 0, '2017-07-10 21:34:12', '2017-07-10 21:34:12'),
(49, 'name', 'Name', 8, 16, 1, '', 1, 250, 1, '', 0, '2017-07-10 21:34:13', '2017-07-10 21:34:13'),
(50, 'display_name', 'Display Name', 8, 19, 0, '', 0, 250, 1, '', 0, '2017-07-10 21:34:13', '2017-07-10 21:34:13'),
(51, 'description', 'Description', 8, 21, 0, '', 0, 1000, 0, '', 0, '2017-07-10 21:34:13', '2017-07-10 21:34:13'),
(53, 'idea', 'Idea', 9, 21, 0, '', 0, 0, 1, '', 2, '2017-07-11 00:19:39', '2017-07-11 20:40:59'),
(54, 'frequency_purchase', 'Usuario Frecuente', 4, 18, 0, '', 0, 0, 0, '[\"Si\",\"No\"]', 0, '2017-07-11 00:23:18', '2017-07-11 00:29:20'),
(55, 'state', 'Estado', 9, 18, 0, 'hidden', 0, 0, 1, '[\"show\",\"hidden\"]', 3, '2017-07-11 00:34:51', '2017-07-11 00:40:05'),
(56, 'rut', 'Rut', 4, 19, 0, '', 10, 10, 0, '', 0, '2017-07-11 00:41:52', '2017-07-11 00:42:05'),
(57, 'user_id', 'Usuario', 9, 7, 0, '', 0, 11, 1, '@users', 1, '2017-07-11 00:43:57', '2017-07-12 00:56:53'),
(58, 'score', 'Puntaje', 9, 13, 0, '0', 0, 11, 0, '', 4, '2017-07-11 00:45:33', '2017-07-11 00:45:33'),
(59, 'idea_id', 'Idea Id', 10, 13, 0, '', 0, 11, 0, '', 0, '2017-07-22 01:26:18', '2017-07-22 01:26:18'),
(60, 'ip', 'ip', 10, 19, 0, '', 0, 256, 0, '', 0, '2017-07-22 01:26:40', '2017-07-22 01:26:40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `module_field_types`
--

CREATE TABLE `module_field_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `module_field_types`
--

INSERT INTO `module_field_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Address', '2017-07-10 21:33:58', '2017-07-10 21:33:58'),
(2, 'Checkbox', '2017-07-10 21:33:58', '2017-07-10 21:33:58'),
(3, 'Currency', '2017-07-10 21:33:58', '2017-07-10 21:33:58'),
(4, 'Date', '2017-07-10 21:33:58', '2017-07-10 21:33:58'),
(5, 'Datetime', '2017-07-10 21:33:58', '2017-07-10 21:33:58'),
(6, 'Decimal', '2017-07-10 21:33:58', '2017-07-10 21:33:58'),
(7, 'Dropdown', '2017-07-10 21:33:58', '2017-07-10 21:33:58'),
(8, 'Email', '2017-07-10 21:33:58', '2017-07-10 21:33:58'),
(9, 'File', '2017-07-10 21:33:58', '2017-07-10 21:33:58'),
(10, 'Float', '2017-07-10 21:33:58', '2017-07-10 21:33:58'),
(11, 'HTML', '2017-07-10 21:33:58', '2017-07-10 21:33:58'),
(12, 'Image', '2017-07-10 21:33:58', '2017-07-10 21:33:58'),
(13, 'Integer', '2017-07-10 21:33:58', '2017-07-10 21:33:58'),
(14, 'Mobile', '2017-07-10 21:33:59', '2017-07-10 21:33:59'),
(15, 'Multiselect', '2017-07-10 21:33:59', '2017-07-10 21:33:59'),
(16, 'Name', '2017-07-10 21:33:59', '2017-07-10 21:33:59'),
(17, 'Password', '2017-07-10 21:33:59', '2017-07-10 21:33:59'),
(18, 'Radio', '2017-07-10 21:33:59', '2017-07-10 21:33:59'),
(19, 'String', '2017-07-10 21:33:59', '2017-07-10 21:33:59'),
(20, 'Taginput', '2017-07-10 21:33:59', '2017-07-10 21:33:59'),
(21, 'Textarea', '2017-07-10 21:33:59', '2017-07-10 21:33:59'),
(22, 'TextField', '2017-07-10 21:33:59', '2017-07-10 21:33:59'),
(23, 'URL', '2017-07-10 21:33:59', '2017-07-10 21:33:59'),
(24, 'Files', '2017-07-10 21:33:59', '2017-07-10 21:33:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `organizations`
--

CREATE TABLE `organizations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'http://',
  `assigned_to` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `connect_since` date NOT NULL,
  `address` varchar(1000) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `city` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `profile_image` int(11) NOT NULL,
  `profile` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `display_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'ADMIN_PANEL', 'Admin Panel', 'Admin Panel Permission', NULL, '2017-07-10 21:34:22', '2017-07-10 21:34:22'),
(2, 'Editores', 'Editor', 'Publicar y/u ocultar información', NULL, '2017-07-10 22:14:59', '2017-07-10 22:14:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(1, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `display_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `parent` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `dept` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `parent`, `dept`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'SUPER_ADMIN', 'Super Admin', 'Full Access Role', 1, 1, NULL, '2017-07-10 21:34:20', '2017-07-10 21:34:20'),
(2, 'EDITOR', 'Editores', '', 1, 2, NULL, '2017-07-10 22:15:26', '2017-07-10 22:15:26'),
(3, 'FRONTUSER', 'Usuarios Front', 'Usuarios que generan ideas', 2, 3, NULL, '2017-07-11 00:27:27', '2017-07-11 00:27:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_module`
--

CREATE TABLE `role_module` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `module_id` int(10) UNSIGNED NOT NULL,
  `acc_view` tinyint(1) NOT NULL,
  `acc_create` tinyint(1) NOT NULL,
  `acc_edit` tinyint(1) NOT NULL,
  `acc_delete` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `role_module`
--

INSERT INTO `role_module` (`id`, `role_id`, `module_id`, `acc_view`, `acc_create`, `acc_edit`, `acc_delete`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 1, 1, '2017-07-10 21:34:20', '2017-07-10 21:34:20'),
(2, 1, 2, 1, 1, 1, 1, '2017-07-10 21:34:20', '2017-07-10 21:34:20'),
(3, 1, 3, 1, 1, 1, 1, '2017-07-10 21:34:21', '2017-07-10 21:34:21'),
(4, 1, 4, 1, 1, 1, 1, '2017-07-10 21:34:21', '2017-07-10 21:34:21'),
(5, 1, 5, 1, 1, 1, 1, '2017-07-10 21:34:21', '2017-07-10 21:34:21'),
(6, 1, 6, 1, 1, 1, 1, '2017-07-10 21:34:22', '2017-07-10 21:34:22'),
(7, 1, 7, 1, 1, 1, 1, '2017-07-10 21:34:22', '2017-07-10 21:34:22'),
(8, 1, 8, 1, 1, 1, 1, '2017-07-10 21:34:22', '2017-07-10 21:34:22'),
(9, 2, 1, 1, 0, 0, 0, '2017-07-10 22:15:26', '2017-07-10 22:15:26'),
(10, 2, 2, 1, 0, 0, 0, '2017-07-10 22:15:27', '2017-07-10 22:15:27'),
(11, 2, 3, 1, 0, 0, 0, '2017-07-10 22:15:27', '2017-07-10 22:15:27'),
(12, 2, 4, 1, 0, 0, 0, '2017-07-10 22:15:27', '2017-07-10 22:15:27'),
(13, 2, 5, 1, 0, 0, 0, '2017-07-10 22:15:28', '2017-07-10 22:15:28'),
(14, 2, 6, 1, 0, 0, 0, '2017-07-10 22:15:28', '2017-07-10 22:15:28'),
(15, 2, 7, 1, 0, 0, 0, '2017-07-10 22:15:28', '2017-07-10 22:15:28'),
(16, 2, 8, 1, 0, 0, 0, '2017-07-10 22:15:28', '2017-07-10 22:15:28'),
(17, 3, 1, 0, 0, 0, 0, '2017-07-11 00:27:27', '2017-07-11 00:27:27'),
(18, 3, 2, 0, 0, 0, 0, '2017-07-11 00:27:27', '2017-07-11 00:27:27'),
(19, 3, 3, 0, 0, 0, 0, '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(20, 3, 4, 0, 0, 0, 0, '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(21, 3, 5, 0, 0, 0, 0, '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(22, 3, 6, 0, 0, 0, 0, '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(23, 3, 7, 0, 0, 0, 0, '2017-07-11 00:27:29', '2017-07-11 00:27:29'),
(24, 3, 8, 0, 0, 0, 0, '2017-07-11 00:27:29', '2017-07-11 00:27:29'),
(25, 3, 9, 0, 0, 0, 0, '2017-07-11 00:27:29', '2017-07-11 00:27:29'),
(26, 1, 9, 1, 1, 1, 1, '2017-07-11 00:33:11', '2017-07-11 00:33:11'),
(27, 2, 9, 1, 0, 1, 1, '2017-07-11 00:33:11', '2017-07-11 00:33:11'),
(28, 1, 10, 1, 1, 1, 1, '2017-07-22 01:27:05', '2017-07-22 01:27:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_module_fields`
--

CREATE TABLE `role_module_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `field_id` int(10) UNSIGNED NOT NULL,
  `access` enum('invisible','readonly','write') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `role_module_fields`
--

INSERT INTO `role_module_fields` (`id`, `role_id`, `field_id`, `access`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'write', '2017-07-10 21:34:20', '2017-07-10 21:34:20'),
(2, 1, 2, 'write', '2017-07-10 21:34:20', '2017-07-10 21:34:20'),
(3, 1, 3, 'write', '2017-07-10 21:34:20', '2017-07-10 21:34:20'),
(4, 1, 4, 'write', '2017-07-10 21:34:20', '2017-07-10 21:34:20'),
(5, 1, 5, 'write', '2017-07-10 21:34:20', '2017-07-10 21:34:20'),
(6, 1, 6, 'write', '2017-07-10 21:34:20', '2017-07-10 21:34:20'),
(7, 1, 7, 'write', '2017-07-10 21:34:20', '2017-07-10 21:34:20'),
(8, 1, 8, 'write', '2017-07-10 21:34:20', '2017-07-10 21:34:20'),
(9, 1, 9, 'write', '2017-07-10 21:34:20', '2017-07-10 21:34:20'),
(10, 1, 10, 'write', '2017-07-10 21:34:20', '2017-07-10 21:34:20'),
(11, 1, 11, 'write', '2017-07-10 21:34:20', '2017-07-10 21:34:20'),
(12, 1, 12, 'write', '2017-07-10 21:34:20', '2017-07-10 21:34:20'),
(13, 1, 13, 'write', '2017-07-10 21:34:21', '2017-07-10 21:34:21'),
(14, 1, 14, 'write', '2017-07-10 21:34:21', '2017-07-10 21:34:21'),
(15, 1, 15, 'write', '2017-07-10 21:34:21', '2017-07-10 21:34:21'),
(16, 1, 16, 'write', '2017-07-10 21:34:21', '2017-07-10 21:34:21'),
(17, 1, 17, 'write', '2017-07-10 21:34:21', '2017-07-10 21:34:21'),
(18, 1, 18, 'write', '2017-07-10 21:34:21', '2017-07-10 21:34:21'),
(19, 1, 19, 'write', '2017-07-10 21:34:21', '2017-07-10 21:34:21'),
(20, 1, 20, 'write', '2017-07-10 21:34:21', '2017-07-10 21:34:21'),
(21, 1, 21, 'write', '2017-07-10 21:34:21', '2017-07-10 21:34:21'),
(22, 1, 22, 'write', '2017-07-10 21:34:21', '2017-07-10 21:34:21'),
(23, 1, 23, 'write', '2017-07-10 21:34:21', '2017-07-10 21:34:21'),
(24, 1, 24, 'write', '2017-07-10 21:34:21', '2017-07-10 21:34:21'),
(25, 1, 25, 'write', '2017-07-10 21:34:21', '2017-07-10 21:34:21'),
(26, 1, 26, 'write', '2017-07-10 21:34:21', '2017-07-10 21:34:21'),
(27, 1, 27, 'write', '2017-07-10 21:34:21', '2017-07-10 21:34:21'),
(28, 1, 28, 'write', '2017-07-10 21:34:21', '2017-07-10 21:34:21'),
(29, 1, 29, 'write', '2017-07-10 21:34:21', '2017-07-10 21:34:21'),
(30, 1, 30, 'write', '2017-07-10 21:34:21', '2017-07-10 21:34:21'),
(31, 1, 31, 'write', '2017-07-10 21:34:21', '2017-07-10 21:34:21'),
(32, 1, 32, 'write', '2017-07-10 21:34:21', '2017-07-10 21:34:21'),
(33, 1, 33, 'write', '2017-07-10 21:34:21', '2017-07-10 21:34:21'),
(34, 1, 34, 'write', '2017-07-10 21:34:21', '2017-07-10 21:34:21'),
(35, 1, 35, 'write', '2017-07-10 21:34:22', '2017-07-10 21:34:22'),
(36, 1, 36, 'write', '2017-07-10 21:34:22', '2017-07-10 21:34:22'),
(37, 1, 37, 'write', '2017-07-10 21:34:22', '2017-07-10 21:34:22'),
(38, 1, 38, 'write', '2017-07-10 21:34:22', '2017-07-10 21:34:22'),
(39, 1, 39, 'write', '2017-07-10 21:34:22', '2017-07-10 21:34:22'),
(40, 1, 40, 'write', '2017-07-10 21:34:22', '2017-07-10 21:34:22'),
(41, 1, 41, 'write', '2017-07-10 21:34:22', '2017-07-10 21:34:22'),
(42, 1, 42, 'write', '2017-07-10 21:34:22', '2017-07-10 21:34:22'),
(43, 1, 43, 'write', '2017-07-10 21:34:22', '2017-07-10 21:34:22'),
(44, 1, 44, 'write', '2017-07-10 21:34:22', '2017-07-10 21:34:22'),
(45, 1, 45, 'write', '2017-07-10 21:34:22', '2017-07-10 21:34:22'),
(46, 1, 46, 'write', '2017-07-10 21:34:22', '2017-07-10 21:34:22'),
(47, 1, 47, 'write', '2017-07-10 21:34:22', '2017-07-10 21:34:22'),
(48, 1, 48, 'write', '2017-07-10 21:34:22', '2017-07-10 21:34:22'),
(49, 1, 49, 'write', '2017-07-10 21:34:22', '2017-07-10 21:34:22'),
(50, 1, 50, 'write', '2017-07-10 21:34:22', '2017-07-10 21:34:22'),
(51, 1, 51, 'write', '2017-07-10 21:34:22', '2017-07-10 21:34:22'),
(52, 2, 1, 'readonly', '2017-07-10 22:15:26', '2017-07-10 22:15:26'),
(53, 2, 2, 'readonly', '2017-07-10 22:15:26', '2017-07-10 22:15:26'),
(54, 2, 3, 'readonly', '2017-07-10 22:15:26', '2017-07-10 22:15:26'),
(55, 2, 4, 'readonly', '2017-07-10 22:15:26', '2017-07-10 22:15:26'),
(56, 2, 5, 'readonly', '2017-07-10 22:15:26', '2017-07-10 22:15:26'),
(57, 2, 6, 'readonly', '2017-07-10 22:15:27', '2017-07-10 22:15:27'),
(58, 2, 7, 'readonly', '2017-07-10 22:15:27', '2017-07-10 22:15:27'),
(59, 2, 8, 'readonly', '2017-07-10 22:15:27', '2017-07-10 22:15:27'),
(60, 2, 9, 'readonly', '2017-07-10 22:15:27', '2017-07-10 22:15:27'),
(61, 2, 10, 'readonly', '2017-07-10 22:15:27', '2017-07-10 22:15:27'),
(62, 2, 11, 'readonly', '2017-07-10 22:15:27', '2017-07-10 22:15:27'),
(63, 2, 12, 'readonly', '2017-07-10 22:15:27', '2017-07-10 22:15:27'),
(64, 2, 13, 'readonly', '2017-07-10 22:15:27', '2017-07-10 22:15:27'),
(65, 2, 14, 'readonly', '2017-07-10 22:15:27', '2017-07-10 22:15:27'),
(66, 2, 15, 'readonly', '2017-07-10 22:15:27', '2017-07-10 22:15:27'),
(67, 2, 16, 'readonly', '2017-07-10 22:15:27', '2017-07-10 22:15:27'),
(68, 2, 17, 'readonly', '2017-07-10 22:15:27', '2017-07-10 22:15:27'),
(69, 2, 18, 'readonly', '2017-07-10 22:15:27', '2017-07-10 22:15:27'),
(70, 2, 19, 'readonly', '2017-07-10 22:15:27', '2017-07-10 22:15:27'),
(71, 2, 20, 'readonly', '2017-07-10 22:15:27', '2017-07-10 22:15:27'),
(72, 2, 21, 'readonly', '2017-07-10 22:15:27', '2017-07-10 22:15:27'),
(73, 2, 22, 'readonly', '2017-07-10 22:15:27', '2017-07-10 22:15:27'),
(74, 2, 23, 'readonly', '2017-07-10 22:15:27', '2017-07-10 22:15:27'),
(75, 2, 24, 'readonly', '2017-07-10 22:15:27', '2017-07-10 22:15:27'),
(76, 2, 25, 'readonly', '2017-07-10 22:15:27', '2017-07-10 22:15:27'),
(77, 2, 26, 'readonly', '2017-07-10 22:15:27', '2017-07-10 22:15:27'),
(78, 2, 27, 'readonly', '2017-07-10 22:15:27', '2017-07-10 22:15:27'),
(79, 2, 28, 'readonly', '2017-07-10 22:15:27', '2017-07-10 22:15:27'),
(80, 2, 29, 'readonly', '2017-07-10 22:15:27', '2017-07-10 22:15:27'),
(81, 2, 30, 'readonly', '2017-07-10 22:15:28', '2017-07-10 22:15:28'),
(82, 2, 31, 'readonly', '2017-07-10 22:15:28', '2017-07-10 22:15:28'),
(83, 2, 32, 'readonly', '2017-07-10 22:15:28', '2017-07-10 22:15:28'),
(84, 2, 33, 'readonly', '2017-07-10 22:15:28', '2017-07-10 22:15:28'),
(85, 2, 34, 'readonly', '2017-07-10 22:15:28', '2017-07-10 22:15:28'),
(86, 2, 35, 'readonly', '2017-07-10 22:15:28', '2017-07-10 22:15:28'),
(87, 2, 36, 'readonly', '2017-07-10 22:15:28', '2017-07-10 22:15:28'),
(88, 2, 37, 'readonly', '2017-07-10 22:15:28', '2017-07-10 22:15:28'),
(89, 2, 38, 'readonly', '2017-07-10 22:15:28', '2017-07-10 22:15:28'),
(90, 2, 39, 'readonly', '2017-07-10 22:15:28', '2017-07-10 22:15:28'),
(91, 2, 40, 'readonly', '2017-07-10 22:15:28', '2017-07-10 22:15:28'),
(92, 2, 41, 'readonly', '2017-07-10 22:15:28', '2017-07-10 22:15:28'),
(93, 2, 42, 'readonly', '2017-07-10 22:15:28', '2017-07-10 22:15:28'),
(94, 2, 43, 'readonly', '2017-07-10 22:15:28', '2017-07-10 22:15:28'),
(95, 2, 44, 'readonly', '2017-07-10 22:15:28', '2017-07-10 22:15:28'),
(96, 2, 45, 'readonly', '2017-07-10 22:15:28', '2017-07-10 22:15:28'),
(97, 2, 46, 'readonly', '2017-07-10 22:15:28', '2017-07-10 22:15:28'),
(98, 2, 47, 'readonly', '2017-07-10 22:15:28', '2017-07-10 22:15:28'),
(99, 2, 48, 'readonly', '2017-07-10 22:15:28', '2017-07-10 22:15:28'),
(100, 2, 49, 'readonly', '2017-07-10 22:15:28', '2017-07-10 22:15:28'),
(101, 2, 50, 'readonly', '2017-07-10 22:15:28', '2017-07-10 22:15:28'),
(102, 2, 51, 'readonly', '2017-07-10 22:15:28', '2017-07-10 22:15:28'),
(103, 1, 53, 'write', '2017-07-11 00:19:40', '2017-07-11 00:19:40'),
(104, 1, 54, 'write', '2017-07-11 00:23:18', '2017-07-11 00:23:18'),
(105, 3, 1, 'invisible', '2017-07-11 00:27:27', '2017-07-11 00:27:27'),
(106, 3, 2, 'invisible', '2017-07-11 00:27:27', '2017-07-11 00:27:27'),
(107, 3, 3, 'invisible', '2017-07-11 00:27:27', '2017-07-11 00:27:27'),
(108, 3, 4, 'invisible', '2017-07-11 00:27:27', '2017-07-11 00:27:27'),
(109, 3, 5, 'invisible', '2017-07-11 00:27:27', '2017-07-11 00:27:27'),
(110, 3, 6, 'invisible', '2017-07-11 00:27:27', '2017-07-11 00:27:27'),
(111, 3, 7, 'invisible', '2017-07-11 00:27:27', '2017-07-11 00:27:27'),
(112, 3, 8, 'invisible', '2017-07-11 00:27:27', '2017-07-11 00:27:27'),
(113, 3, 9, 'invisible', '2017-07-11 00:27:27', '2017-07-11 00:27:27'),
(114, 3, 10, 'invisible', '2017-07-11 00:27:27', '2017-07-11 00:27:27'),
(115, 3, 11, 'invisible', '2017-07-11 00:27:27', '2017-07-11 00:27:27'),
(116, 3, 12, 'invisible', '2017-07-11 00:27:27', '2017-07-11 00:27:27'),
(117, 3, 13, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(118, 3, 14, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(119, 3, 15, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(120, 3, 16, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(121, 3, 17, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(122, 3, 18, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(123, 3, 19, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(124, 3, 20, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(125, 3, 21, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(126, 3, 22, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(127, 3, 23, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(128, 3, 24, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(129, 3, 25, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(130, 3, 26, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(131, 3, 27, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(132, 3, 28, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(133, 3, 29, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(134, 3, 54, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(135, 3, 30, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(136, 3, 31, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(137, 3, 32, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(138, 3, 33, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(139, 3, 34, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(140, 3, 35, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(141, 3, 36, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(142, 3, 37, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(143, 3, 38, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(144, 3, 39, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(145, 3, 40, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(146, 3, 41, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(147, 3, 42, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(148, 3, 43, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(149, 3, 44, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(150, 3, 45, 'invisible', '2017-07-11 00:27:28', '2017-07-11 00:27:28'),
(151, 3, 46, 'invisible', '2017-07-11 00:27:29', '2017-07-11 00:27:29'),
(152, 3, 47, 'invisible', '2017-07-11 00:27:29', '2017-07-11 00:27:29'),
(153, 3, 48, 'invisible', '2017-07-11 00:27:29', '2017-07-11 00:27:29'),
(154, 3, 49, 'invisible', '2017-07-11 00:27:29', '2017-07-11 00:27:29'),
(155, 3, 50, 'invisible', '2017-07-11 00:27:29', '2017-07-11 00:27:29'),
(156, 3, 51, 'invisible', '2017-07-11 00:27:29', '2017-07-11 00:27:29'),
(157, 3, 53, 'invisible', '2017-07-11 00:27:29', '2017-07-11 00:27:29'),
(158, 2, 54, 'invisible', '2017-07-11 00:32:01', '2017-07-11 00:32:01'),
(159, 2, 53, 'write', '2017-07-11 00:33:11', '2017-07-11 00:33:11'),
(160, 1, 55, 'write', '2017-07-11 00:34:51', '2017-07-11 00:34:51'),
(161, 1, 56, 'write', '2017-07-11 00:41:52', '2017-07-11 00:41:52'),
(162, 1, 57, 'write', '2017-07-11 00:43:57', '2017-07-11 00:43:57'),
(163, 1, 58, 'write', '2017-07-11 00:45:34', '2017-07-11 00:45:34'),
(164, 2, 55, 'write', '2017-07-11 20:36:48', '2017-07-11 20:36:48'),
(165, 2, 57, 'readonly', '2017-07-11 20:36:48', '2017-07-11 20:36:48'),
(166, 2, 58, 'readonly', '2017-07-11 20:36:48', '2017-07-11 20:36:48'),
(167, 3, 55, 'invisible', '2017-07-11 20:36:48', '2017-07-11 20:36:48'),
(168, 3, 57, 'invisible', '2017-07-11 20:36:48', '2017-07-11 20:36:48'),
(169, 3, 58, 'invisible', '2017-07-11 20:36:48', '2017-07-11 20:36:48'),
(170, 1, 59, 'write', '2017-07-22 01:26:19', '2017-07-22 01:26:19'),
(171, 1, 60, 'write', '2017-07-22 01:26:41', '2017-07-22 01:26:41');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(2, 1, 1, NULL, NULL),
(14, 2, 13, NULL, NULL),
(17, 3, 16, NULL, NULL),
(18, 3, 17, NULL, NULL),
(19, 3, 18, NULL, NULL),
(20, 3, 19, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `uploads`
--

CREATE TABLE `uploads` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `path` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `extension` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `caption` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `hash` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `public` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `uploads`
--

INSERT INTO `uploads` (`id`, `name`, `path`, `extension`, `caption`, `user_id`, `hash`, `public`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Cotización landing Abastible.pdf', '/var/www/html/abastible-mejoremos-el-mundo/site/storage/uploads/2017-07-10-181108-Cotización landing Abastible.pdf', 'pdf', '', 1, 'tw1ixztzwiabo0zyt6jx', 0, '2017-07-10 22:11:47', '2017-07-10 22:11:08', '2017-07-10 22:11:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `context_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Employee',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `context_id`, `email`, `password`, `type`, `remember_token`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'admin', 1, 'claudio@growthy.cl', '$2y$10$7LtZfgO6OwvhjeNv9WqEkOz5Kp2/7es96flRSnwzObJPP/s3QChA6', 'Employee', '4PTLxLP1ee80dFi7SE4TMnDaqPEjHKdNcFF7zo7BApP5DxVuOuoUtVIjsLg5', NULL, '2017-07-10 21:34:51', '2017-07-12 01:06:45'),
(13, 'Mauricio Marambio', 13, 'mauricio.marambio@dyu.cl', '$2y$10$U/oa2rSUxf8lFaxAHIE2xuI6hiCEOlwP4PYzFoH3B7a.PANmxlUDG', 'Employee', NULL, NULL, '2017-07-12 20:34:00', '2017-07-12 20:34:00'),
(16, 'testds fdsf ', 16, 'felipe@growthy.cl', '$2y$10$YbFbizgJWTXYDs4EzC46fuz9RKMvHAWFe0stDcqmEOQBJcVo16Ma.', 'Employee', NULL, NULL, '2017-07-12 22:36:15', '2017-07-12 22:36:15'),
(17, 'Ddddddddddddddd', 17, '33333@3333.com', '$2y$10$UwNsdehQ/oXBeWqIzmjWNOI09FArqlCY/uWx.XVLVms6UX10dE22C', 'Employee', NULL, NULL, '2017-07-13 01:08:51', '2017-07-13 01:08:51'),
(18, '899999999999999', 18, '44444@555.com', '$2y$10$G6/EWbrO2bfHxxtLodozE.5V84VcvLR1xp57lFJL.nlaavtTmYpjW', 'Employee', NULL, NULL, '2017-07-13 01:10:28', '2017-07-13 01:10:28'),
(19, '555555555', 19, '555555@55555.com', '$2y$10$PrqyG/J6u5Hfu8c2K/MKt.Qk/LIEFJaVjOc5UAq01XiCFm2UA5G3a', 'Employee', NULL, NULL, '2017-07-13 01:11:08', '2017-07-13 01:11:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `votes`
--

CREATE TABLE `votes` (
  `id` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `idea_id` int(10) UNSIGNED NOT NULL,
  `ip` varchar(256) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `votes`
--

INSERT INTO `votes` (`id`, `deleted_at`, `created_at`, `updated_at`, `idea_id`, `ip`) VALUES
(9, NULL, '2017-07-22 01:35:11', '2017-07-22 01:35:11', 19, '::1'),
(10, NULL, '2017-07-22 01:35:11', '2017-07-22 01:35:11', 18, '::1'),
(11, NULL, '2017-07-22 01:35:12', '2017-07-22 01:35:12', 17, '::1');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `backups`
--
ALTER TABLE `backups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `backups_name_unique` (`name`),
  ADD UNIQUE KEY `backups_file_name_unique` (`file_name`);

--
-- Indices de la tabla `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `departments_name_unique` (`name`);

--
-- Indices de la tabla `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employees_email_unique` (`email`),
  ADD KEY `employees_dept_foreign` (`dept`);

--
-- Indices de la tabla `ideas`
--
ALTER TABLE `ideas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `la_configs`
--
ALTER TABLE `la_configs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `la_menus`
--
ALTER TABLE `la_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `module_fields`
--
ALTER TABLE `module_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `module_fields_module_foreign` (`module`),
  ADD KEY `module_fields_field_type_foreign` (`field_type`);

--
-- Indices de la tabla `module_field_types`
--
ALTER TABLE `module_field_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `organizations`
--
ALTER TABLE `organizations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `organizations_name_unique` (`name`),
  ADD UNIQUE KEY `organizations_email_unique` (`email`),
  ADD KEY `organizations_assigned_to_foreign` (`assigned_to`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indices de la tabla `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`),
  ADD KEY `roles_parent_foreign` (`parent`),
  ADD KEY `roles_dept_foreign` (`dept`);

--
-- Indices de la tabla `role_module`
--
ALTER TABLE `role_module`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_module_role_id_foreign` (`role_id`),
  ADD KEY `role_module_module_id_foreign` (`module_id`);

--
-- Indices de la tabla `role_module_fields`
--
ALTER TABLE `role_module_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_module_fields_role_id_foreign` (`role_id`),
  ADD KEY `role_module_fields_field_id_foreign` (`field_id`);

--
-- Indices de la tabla `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `uploads`
--
ALTER TABLE `uploads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uploads_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indices de la tabla `votes`
--
ALTER TABLE `votes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `backups`
--
ALTER TABLE `backups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de la tabla `ideas`
--
ALTER TABLE `ideas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de la tabla `la_configs`
--
ALTER TABLE `la_configs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `la_menus`
--
ALTER TABLE `la_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `module_fields`
--
ALTER TABLE `module_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT de la tabla `module_field_types`
--
ALTER TABLE `module_field_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT de la tabla `organizations`
--
ALTER TABLE `organizations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `role_module`
--
ALTER TABLE `role_module`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT de la tabla `role_module_fields`
--
ALTER TABLE `role_module_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=172;
--
-- AUTO_INCREMENT de la tabla `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT de la tabla `uploads`
--
ALTER TABLE `uploads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de la tabla `votes`
--
ALTER TABLE `votes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employees_dept_foreign` FOREIGN KEY (`dept`) REFERENCES `departments` (`id`);

--
-- Filtros para la tabla `module_fields`
--
ALTER TABLE `module_fields`
  ADD CONSTRAINT `module_fields_field_type_foreign` FOREIGN KEY (`field_type`) REFERENCES `module_field_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `module_fields_module_foreign` FOREIGN KEY (`module`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `organizations`
--
ALTER TABLE `organizations`
  ADD CONSTRAINT `organizations_assigned_to_foreign` FOREIGN KEY (`assigned_to`) REFERENCES `employees` (`id`);

--
-- Filtros para la tabla `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `roles`
--
ALTER TABLE `roles`
  ADD CONSTRAINT `roles_dept_foreign` FOREIGN KEY (`dept`) REFERENCES `departments` (`id`),
  ADD CONSTRAINT `roles_parent_foreign` FOREIGN KEY (`parent`) REFERENCES `roles` (`id`);

--
-- Filtros para la tabla `role_module`
--
ALTER TABLE `role_module`
  ADD CONSTRAINT `role_module_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_module_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `role_module_fields`
--
ALTER TABLE `role_module_fields`
  ADD CONSTRAINT `role_module_fields_field_id_foreign` FOREIGN KEY (`field_id`) REFERENCES `module_fields` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_module_fields_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `uploads`
--
ALTER TABLE `uploads`
  ADD CONSTRAINT `uploads_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
